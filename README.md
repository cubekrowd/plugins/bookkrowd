# BookKrowd

A plugin for making formatting books more accessible. Automatically processes books when signed to replace chat colors (i.e. &a) with colors, has a way to import a book via URl and can edit signed books.

## Commands and Permissions

---
#### Format Books
**Permission**: bookkrowd.format
Formats books when signed.

---
#### /bookkrowd /bk /book
**Permission**: bookkrowd.help

Displays help command.

---
#### /bookkrowd help [\<commandname\>]
**Permission**: bookkrowd.help

Provides information on how to use commands. 

---
#### /bookkrowd about
**Permission**: none

Gives basic information about the plugin.

---
#### /bookkrowd import \<url\> [\<title\>]
**Permission**: bookkrowd.import

Imports a book from a ghostbin.co URL, gist.github.com, hastebin.com, or pastebin.com URL. Chat colors work inside and page breaks. To make a page break put in 3 enters, so there will be 2 lines of space between text. Author is set as the player who ran the command and if no title is specified, just the player name.
*Limit on characters is 2000 and page limit is 30*

---
#### /bookkrowd edit [plain/colored/colorcodes]
**Permission**: bookkrowd.edit

Copies signed book in hand to editable form. Used for editing books. Default way of copying is plain. Plain removes all color codes. Colored keeps all colors. Colorcodes changes colors to their colorcodes.

---
#### /bookkrowd reload
**Permission**: bookkrowd.reload

Reloads the configuration file.

---


## Configuration

---
**config.yml** Inside config.yml you have 3 seperate values. *charactermax, pagemax* and *sizemax*. Charactermax is the maximum of characters allowed inside of the book. Pagemax is the maximum amount of pages allowed. Sizemax is the maximum size a request can be.
