/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


package net.cubekrowd.bookkrowd;

import org.bukkit.plugin.java.JavaPlugin;


public final class BookKrowd extends JavaPlugin {


    @Override
    public void onEnable() {
        getCommand("bookkrowd").setExecutor(new BookKrowdCommand(this));
        getServer().getPluginManager().registerEvents(new EventListener(), this);

        loadConfig();

    }

    public void loadConfig(){
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();

    }

}
