/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd;

import net.cubekrowd.bookkrowd.subcommands.AboutCommand;
import net.cubekrowd.bookkrowd.subcommands.EditCommand;
import net.cubekrowd.bookkrowd.subcommands.HelpCommand;
import net.cubekrowd.bookkrowd.subcommands.ImportCommand;
import net.cubekrowd.bookkrowd.subcommands.ReloadCommand;
import net.cubekrowd.bookkrowd.subcommands.SubCommand;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;


public class BookKrowdCommand implements CommandExecutor {

    private final BookKrowd plugin;

    // Easy to reference chat messages
    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;
    private final static String INVALID_ARG = PREFIX+ChatColor.RED+"Invalid arguments. Use /bookkrowd help.";
    private final static String INVALID_PERMS = PREFIX+ChatColor.RED+"You do not have permission to do that.";


    private HashMap<String, SubCommand> subCommands = new HashMap<>();

    public BookKrowdCommand(BookKrowd plugin){
        this.plugin = plugin;
        EditCommand edit = new EditCommand();
        ImportCommand importcommand = new ImportCommand(plugin);
        ReloadCommand reloadcommand = new ReloadCommand(plugin, getSubCommands());
        AboutCommand aboutcommand = new AboutCommand(plugin);
        HelpCommand helpcommand = new HelpCommand(getSubCommands());

        subCommands.put(edit.getName(), edit);
        subCommands.put(importcommand.getName(), importcommand);
        subCommands.put(reloadcommand.getName(), reloadcommand);
        subCommands.put(aboutcommand.getName(), aboutcommand);
        subCommands.put(helpcommand.getName(), helpcommand);

    }




    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, this command is only for players!");
            return true;
        }

        Player player = (Player) sender;
        if(args.length == 0){
            args = new String[] {"help"};
        }
        // After parsing through small misc commands, we then check for them in the subcommands.
        SubCommand subcommand = getSubCommands().get(args[0]);
        if(subcommand != null) {
            // Checks to see the permission, if the permission is null, then there isn't a permission requirement for the command.
            String permission = subcommand.getPermission();
            if (permission == null || player.hasPermission(permission)) {
                // Used to simplify the other classes, checks the make sure that there are the required amount of arguments.
                int maxargs = subcommand.getMaxArgs();
                int minargs = subcommand.getMinArgs();

                if((args.length >= minargs) && (maxargs == 0 || args.length <= maxargs)) {
                    subcommand.perform(player, args);
                    return true;

                } else {
                    // Since we know what command they tried, we can send them the correct usage.
                    player.sendMessage(INVALID_ARG+" Correct usage: "+ChatColor.GRAY+subcommand.getUsage());
                    return true;

                }

            } else {
                player.sendMessage(INVALID_PERMS);
                return true;

            }
        } else {
            player.sendMessage(INVALID_ARG);
            return false;
        }

    }


    public HashMap<String, SubCommand> getSubCommands() {
        return subCommands;
    }


}