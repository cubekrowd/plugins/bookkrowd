/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/





package net.cubekrowd.bookkrowd;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class BookFormatter {

    public static List<String> addColors(List<String> pages){
        /*
        Takes a list of strings (the different pages)
        and formats all of the color codes using Bukkit's built in formatter.
        Then it returns it back.
         */

        List<String> newPages = new ArrayList<>();
        for(String page : pages){
            page = ChatColor.translateAlternateColorCodes('&', page);
            newPages.add(page);
        }
        return newPages;
    }


    public static List<String> removeColors(List<String> pages){
        /*
        Takes a list of strings (the different pages)
        and formats all of the color codes using Bukkit's built in formatter.
        Then it returns it back.
         */

        List<String> newPages = new ArrayList<>();
        for(String page : pages){
            page = ChatColor.stripColor( page);
            newPages.add(page);
        }
        return newPages;
    }


    public static List<String> addColorCodes(List<String> pages){
        /*
        Takes a list of strings (the different pages)
        and replaces the chat color character with &.
        Then it returns it back.
         */

        List<String> newPages = new ArrayList<>();
        for(String page : pages){
            String pageData = page.replace(ChatColor.COLOR_CHAR, '&');

            // The reason for this replacement of &0\n is because Minecraft resets all syntax at the start of the new line. When formatting it includes this restart syntax, this removes it.
            pageData = pageData.replace("&0\n", "\n");
            newPages.add(pageData);
        }
        return newPages;
    }

}
