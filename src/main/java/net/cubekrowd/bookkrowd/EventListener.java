/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.meta.BookMeta;

import java.util.List;

public class EventListener implements Listener {


    @EventHandler(ignoreCancelled = true)
    public void editBook(PlayerEditBookEvent e){
        // Checks to see if the book is being signed.
        if(!e.isSigning()) return;


        Player player = e.getPlayer();
        if(!player.hasPermission("bookkrowd.format")){
            return;
        }

        // Takes the pages from the books and formats it using BookFormatter.java
        BookMeta book = e.getNewBookMeta();
        List<String> newPages = BookFormatter.addColors(book.getPages());

        book.setPages(newPages);
        // If there is a title it will try to use it's color codes.
        if(book.getTitle() != null) {
            book.setTitle(ChatColor.translateAlternateColorCodes('&', book.getTitle()));
        }
        e.setNewBookMeta(book);

    }



}
