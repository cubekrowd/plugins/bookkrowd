/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import org.bukkit.entity.Player;

public abstract class SubCommand {
    /*
    https://www.youtube.com/watch?v=WyFN_jTS4nU&list=PLfu_Bpi_zcDNEKmR82hnbv9UxQ16nUBF7&index=52
    Command handling system based off of Kody Simpson Command Manager.
     */

    // Declares necessary functions for each subcommand.
    public abstract void reload();

    public abstract String getName();

    public abstract int getMaxArgs();

    public abstract int getMinArgs();

    public abstract String getUsage();

    public abstract String getPermission();

    public abstract void perform(Player player, String[] args);

    public abstract String getSmallDescription();

    public abstract String getDetailedDescription();

}
