/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import java.io.IOException;
import java.util.logging.Level;
import net.cubekrowd.bookkrowd.BookFormatter;
import net.cubekrowd.bookkrowd.BookKrowd;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class ImportCommand extends SubCommand {
    // Easy to reference chat messages
    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;

    private final static String INVALID_PERMS = PREFIX+ChatColor.RED+"You do not have permission to do that.";

    // This is to store the url so that it can be used inside the command function.
    private int maxcharacters;
    private int maxpages;
    private int maxsize;

    private static final String[] ALLOWED_HOSTS = {"gist.githubusercontent.com", "ghostbin.co", "hastebin.com", "pastebin.com"};

    private final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    private final BookKrowd plugin;


    @Override
    public String getSmallDescription(){
        return "Imports a book based off of a URL.";
    }

    @Override
    public String getDetailedDescription(){
        return "Imports a book from a URL. Supports gist.github.com, ghostbin.co, hastebin.com, and pastebin.com links. All chat color codes work (i.e. &6). To add a new page, put in 3 enters where you want it (so that there is a two line space). The third argument is the title. This works with chat colors, the limit is 256 characters.";
    }


    public ImportCommand (BookKrowd plugin) {
        this.plugin = plugin;
        setMax();
    }

    @Override
    public String getPermission(){
        return "bookkrowd.import";
    }

    @Override
    public String getUsage(){
        return "/bookkrowd import <url> [<title>]";
    }

    @Override
    public void reload(){
        setMax();
    }

    @Override
    public String getName() {
        return "import";
    }

    @Override
    public int getMinArgs(){
        return 2;
    }

    @Override
    public int getMaxArgs(){
        return 0;
    }



    public void setMax() {
        maxcharacters = plugin.getConfig().getInt("charactermax");
        maxpages = plugin.getConfig().getInt("pagemax");
        maxsize = plugin.getConfig().getInt("sizemax");

    }


    private String getText(Player player, String url, int limit)
            throws IOException, InterruptedException {
        /*
        partly used https://www.baeldung.com/convert-input-stream-to-string
         */

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url))
                .timeout(Duration.ofSeconds(15))
                .build();

        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());

        if (response.statusCode() != 200) {
            plugin.getServer().getScheduler().runTask(plugin, () -> {
                player.sendMessage(PREFIX + ChatColor.RED
                        + "That URL led to the HTTP status code "
                        + response.statusCode() + ".");
            });
            return null;
        }

        try (InputStream stream = response.body()) {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[1024];
            long size = 0;
            int chunk = 0;
            while ((chunk = stream.read(data, 0, data.length)) != -1) {
                size += chunk;
                if (size > limit) {
                    plugin.getServer().getScheduler().runTask(plugin, () -> {
                        player.sendMessage(PREFIX + ChatColor.RED
                                + "Your URL exceeded the size limit ("
                                + maxsize + " bytes).");
                    });
                    return null;
                }
                buffer.write(data, 0, chunk);
            }

            byte[] byteArray = buffer.toByteArray();
            return new String(byteArray, StandardCharsets.UTF_8);
        }

    }


    public boolean checkURL(String url){
        try {
            URL testURL = new URL(url);
            String host = testURL.getHost();

            boolean allowed = false;
            for(String hosts : ALLOWED_HOSTS){
                if(host.equalsIgnoreCase(hosts)){
                    allowed = true;
                    break;
                }
            }
            if(!allowed){
                return false;
            }


        }catch (MalformedURLException e) {
            return false;
        }
        return true;


    }

    @Override
    public void perform(Player player, String[] args) {

        // Grabs the url from argument 1.
        String url = args[1];
        player.sendMessage(PREFIX+"Building book data...");

        // Checks to see if it's a compatable url, if it is then it formats it to get raw info.
        if (url.contains("ghostbin.co")) {
            url = url + "/raw";

        }else if (url.contains("gist.github.com")) {
            url = url + "/raw";

            // The reason for this line of code is because gist.github.com/user/gist/raw redirects from gist.github to gist.githubusercontent for some reason.
            // This is just to make it so we don't have to go through a redirect.
            url = url.replace("github","githubusercontent");
        }
        else if (url.contains("hastebin.com")) {
            url = url.replace("hastebin.com/", "hastebin.com/raw/");

        }
        else if (url.contains("pastebin.com")) {
            url = url.replace("pastebin.com/", "pastebin.com/raw/");


        }
        else {
            player.sendMessage(ChatColor.RED+"URL not supported");
            return;
        }

        // Lets the player know where the text is being grabbed from.
        player.sendMessage(PREFIX+url);

        if (!checkURL(url)) {
            player.sendMessage(PREFIX+ChatColor.RED+"Your URL is not valid!");
            return;
        }


        final String finalUrl = url;
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            // Only part that needs to be done Async.
            String result;
            try {
                result = getText(player, finalUrl, maxsize);
            } catch (Exception e) {
                Bukkit.getScheduler().runTask(plugin, () ->{
                    player.sendMessage(PREFIX+ChatColor.RED+"An error happened when getting the data.");
                    plugin.getLogger().log(Level.WARNING, "Failed to process GET request", e);
                });
                return;
            }

            if (result == null) {
                // something went wrong
                return;
            }

            Bukkit.getScheduler().runTask(plugin, () -> {
                // Goes back to sync on the main Bukkit thread.
                // Symbol that can appear inside the webpages. Minecraft doesn't format it correctly so it's deleted.
                String editedresult = result.replaceAll("\r", "");

                // The max amount of characters allowed.
                if (editedresult.length()>maxcharacters) {
                    player.sendMessage(PREFIX+ChatColor.RED+"There are too many characters in the request ("+editedresult.length()+"). Max is "+maxcharacters+".");
                    return;
                }else if (editedresult.length()<10) {
                    // Just in case it's possible to send a blank value in.
                    player.sendMessage(PREFIX+ChatColor.RED+"You need to have at least 10 characters in the request. Could have been an invalid URL.\n Result: "+editedresult);
                    return;
                }

                // Formats page breaks with 3 returns.
                String[] finalResult = editedresult.split("\n\n\n");

                // Max amount of pages is 30, any more and it will stop.
                if (finalResult.length > maxpages) {
                    player.sendMessage(PREFIX+ChatColor.RED+"There are too many pages in the request ("+finalResult.length+"). Max is "+maxpages+".");
                    return;

                }

                // After passing all of the prerequisites it starts the book making process.
                ItemStack book = new ItemStack(Material.WRITTEN_BOOK, 1);
                BookMeta bookmeta = (BookMeta)book.getItemMeta();

                // Formats all the pages.
                List<String> formatted = Arrays.asList(finalResult);
                formatted= BookFormatter.addColors(formatted);
                bookmeta.setPages(formatted);
                bookmeta.setAuthor(player.getName());

                // For the title I added the option to include it after the url argument.
                if (args.length == 2) {
                    bookmeta.setTitle(player.getName());
                }
                else {
                    String title = "";
                    int loop = args.length - 2;
                    for(int i = 1; i <= loop; i++){
                        title = title + args[i+1]+" ";
                    }
                    bookmeta.setTitle(ChatColor.translateAlternateColorCodes('&', title));
                    if(title.length()>256) {
                        player.sendMessage(PREFIX+ChatColor.RED+"Your title was over the limit of 256 characters.");
                        return;
                    }
                }

                book.setItemMeta(bookmeta);
                player.getInventory().addItem(book);
                player.sendMessage(PREFIX+"Done!");

            });

        });

    }



}
