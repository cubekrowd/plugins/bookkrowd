/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import net.cubekrowd.bookkrowd.BookKrowd;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ReloadCommand extends SubCommand {


    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;

    private final BookKrowd plugin;
    private final HashMap<String, SubCommand> commands;

    public ReloadCommand (BookKrowd plugin, HashMap<String, SubCommand> commands) {
        this.plugin = plugin;
        this.commands = commands;
    }

    @Override
    public String getSmallDescription(){
        return "Reloads the config.";
    }

    @Override
    public String getDetailedDescription(){
        return "Reloads the config. Grabs the new maxcharacters, maxpages, and maxsize values.";
    }


    @Override
    public void reload(){}

    @Override
    public String getPermission(){
        return "bookkrowd.reload";
    }

    @Override
    public String getUsage(){
        return "/bookkrowd reload";
    }

    @Override
    public String getName(){
        return "reload";
    }

    @Override
    public int getMinArgs(){
        return 1;
    }

    @Override
    public int getMaxArgs(){
        return 1;
    }


    @Override
    public void perform(Player player, String[] args){
        // Incase there are any variables that need to be reset on config reload.
        plugin.reloadConfig();

        for(String substring : commands.keySet()) {
            SubCommand subcommand = commands.get(substring);
            subcommand.reload();
        }

        player.sendMessage(PREFIX+"Reloaded!");
    }

}
