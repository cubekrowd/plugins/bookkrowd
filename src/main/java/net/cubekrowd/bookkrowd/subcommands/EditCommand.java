/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import net.cubekrowd.bookkrowd.BookFormatter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.List;

public class EditCommand extends SubCommand{

    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;
    private final static String INVALID_PERMS = PREFIX+ChatColor.RED+"You do not have permission to do that.";
    private final static String INVALID_ARG = PREFIX+ChatColor.RED+"Invalid arguments. Use /bookkrowd help.";

    @Override
    public String getSmallDescription(){
        return "Copies the book on your hand.";
    }

    @Override
    public String getDetailedDescription(){
        return "Copies the book on your hand. Default is plain formatting. Plain strips all colors from the book. Colored leaves in all of the colors. Colorcodes replaces all of the colors with their respected color codes.";
    }


    @Override
    public void reload(){}

    @Override
    public String getPermission(){
        return "bookkrowd.edit";
    }

    @Override
    public String getUsage(){
        return "/bookkrowd edit [plain/colored/colorcodes]";
    }

    @Override
    public String getName(){
        return "edit";
    }

    @Override
    public int getMinArgs(){
        return 1;
    }

    @Override
    public int getMaxArgs(){
        return 2;
    }


    @Override
    public void perform (Player player, String[] args) {


        // Checks first to see if item in hand is a signed book.
        if (player.getInventory().getItemInMainHand().getType() == Material.WRITTEN_BOOK) {
            player.sendMessage(PREFIX + "Copying book data in hand...");
        }else {
            player.sendMessage(PREFIX+ ChatColor.RED+"The item in your hand has to be a signed book.");
            return;
        }

        // CloneBook is the book inside of the players hand, newbook is a new book to write data to.
        ItemStack clonebook = player.getInventory().getItemInMainHand();
        BookMeta clonemeta = (BookMeta)clonebook.getItemMeta();
        ItemStack newbook = new ItemStack(Material.WRITABLE_BOOK, 1);
        BookMeta newmeta = (BookMeta) newbook.getItemMeta();

        // Checks to see if there are pages inside book, if so it then copies them over.
        if (!clonemeta.getPages().isEmpty()) {
            if (args.length == 2 && !args[1].equalsIgnoreCase("plain")) {
                if (args[1].equalsIgnoreCase("colored")) {
                    newmeta.setPages(clonemeta.getPages());

                }
                else if (args[1].equalsIgnoreCase("colorcodes")) {
                    List<String> toreplace = clonemeta.getPages();
                    List<String> formatted = BookFormatter.addColorCodes(toreplace);
                    newmeta.setPages(formatted);

                } else {
                    player.sendMessage(INVALID_ARG);
                    return;
                }


            }else {
                // This is to strip all formatting, because it can cause crashes.
                List<String> tostrip = clonemeta.getPages();
                List<String> formatted = BookFormatter.removeColors(tostrip);
                newmeta.setPages(formatted);
            }
        }
        else {
            player.sendMessage(PREFIX+ChatColor.RED+"Invalid book.");
            return;
        }

        newbook.setItemMeta(newmeta);
        player.getInventory().addItem(newbook);
        player.sendMessage(PREFIX+"Done!");
        // This keeps all color codes so is easy for a quick edit of a book. I've had to keep numerous copies of books to just edit it, now it's easy!

    }


}
