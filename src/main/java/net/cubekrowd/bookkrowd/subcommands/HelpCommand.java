/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class HelpCommand extends SubCommand {

    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;
    private final static String LINEBREAK = ChatColor.DARK_GRAY+""+ChatColor.STRIKETHROUGH+"[                              ]";
    private final static String INVALID_ARG = PREFIX+ChatColor.RED+"Invalid arguments. Use /bookkrowd help";

    private final HashMap<String, SubCommand> commands;

    public HelpCommand(HashMap<String, SubCommand> commands){
        this.commands = commands;
    }

    @Override
    public String getSmallDescription(){
        return "Shows commands for BookKrowd.";
    }

    @Override
    public String getDetailedDescription(){
        return "Shows commands for BookKrowd.";
    }


    @Override
    public void reload(){}

    @Override
    public String getPermission(){
        return "bookkrowd.help";
    }

    @Override
    public String getUsage(){
        return "/bookkrowd help [<commandname>]";
    }

    @Override
    public String getName(){
        return "help";
    }

    @Override
    public int getMinArgs(){
        return 0;
    }

    @Override
    public int getMaxArgs(){
        return 2;
    }


    @Override
    public void perform(Player player, String[] args){

        if (args.length==2) {
            SubCommand commandclass = commands.get(args[1]);
            if (commandclass == null) {
                player.sendMessage(INVALID_ARG);
                return;
            }

            String message = LINEBREAK+"\n"+ChatColor.WHITE+"     "+commandclass.getUsage()+"\n"+
                    ChatColor.GRAY+commandclass.getDetailedDescription()+"\n"+LINEBREAK;
            player.sendMessage(message);
        }
        else {
            StringBuilder message = new StringBuilder(LINEBREAK+"\n");
            for (String scommand : commands.keySet()) {
                SubCommand commandclass = commands.get(scommand);
                message.append(ChatColor.WHITE+"  "+commandclass.getUsage()+ChatColor.GRAY+" "+commandclass.getSmallDescription()+"\n");
            }
            message.append(LINEBREAK);
            player.sendMessage(message.toString());

        }



    }

}
