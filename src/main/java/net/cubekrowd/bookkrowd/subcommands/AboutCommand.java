/*

    BookKrowd
    Copyright (C) 2020 DarkKronicle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



package net.cubekrowd.bookkrowd.subcommands;

import net.cubekrowd.bookkrowd.BookKrowd;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class AboutCommand extends SubCommand {

    private final static String PREFIX = ChatColor.LIGHT_PURPLE+"[BK] "+ChatColor.GRAY;

    private final BookKrowd plugin;

    public AboutCommand (BookKrowd plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getSmallDescription(){
        return "Gets the basic information about the plugin.";
    }

    @Override
    public String getDetailedDescription(){
        return "Gets the author, name, and version of the plugin.";
    }


    @Override
    public void reload(){}

    @Override
    public String getPermission(){
        return null;
    }

    @Override
    public String getUsage(){
        return "/bookkrowd about";
    }

    @Override
    public String getName(){
        return "about";
    }

    @Override
    public int getMinArgs(){
        return 1;
    }

    @Override
    public int getMaxArgs(){
        return 1;
    }


    @Override
    public void perform(Player player, String[] args){
        player.sendMessage(PREFIX + "BookKrowd was made by DarkKronicle. Current Version is: " + ChatColor.LIGHT_PURPLE + "[" + plugin.getDescription().getVersion() + "]");

    }

}
